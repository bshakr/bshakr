class Post < ActiveRecord::Base
  validates_presence_of :title, :content, :published_at
  validates :slug, uniqueness: true, presence: true
  before_validation :generate_slug
  
  #defines the database field to be used in the url paramater
  def to_param
  	slug
  end
  
  def generate_slug
  	self.slug ||= title.parameterize
  end
  
  def render_html
    options = {
      no_intra_emphasis: true,
      tables: true,
      fenced_code_blocks: true,
      autolink: true,
      strikethrough: true,
      space_after_headers: true,
      superscript: true
    }
    markdown = Redcarpet::Markdown.new(Bshakr::MarkdownRenderer, options)
    markdown.render(self.content).html_safe
  end

  def self.search(search)
  	if search
  		where('title LIKE ?', "%#{search}%")
  	else
  		unscoped
  	end
  end

end
