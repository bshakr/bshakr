module PostsHelper
  def post_published_time post
    post.published_at.to_time.strftime('%B %e, %Y')
  end
end
