atom_feed do |feed|
  feed.title "Bassem Shaker Blog"
  feed.updated @posts.maximum(:published_at)
  @posts.each do |post|
    feed.entry post, published: post.published_at do |entry|
      entry.title post.title
      entry.content post.content
      entry.author do |author|
        author.name "Bassem Shaker"
      end
    end
  end
  
end
