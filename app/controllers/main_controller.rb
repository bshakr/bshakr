class MainController < ApplicationController
  def home
	  @posts = Post.where("published_at <= ?", Time.now).order("published_at DESC")
  end
  
  def about
    @instagram = Instagram.user_recent_media("4352736", {:count => 6})
  end
end
