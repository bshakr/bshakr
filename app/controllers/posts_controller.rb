class PostsController < ApplicationController
  respond_to :html, :xml, :json, :atom
  def index
	  @posts = Post.where("published_at <= ?", Time.now).order("published_at DESC")
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  	@post = Post.find_by_slug!(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end
end
