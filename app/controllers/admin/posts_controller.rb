class Admin::PostsController < AdminController
	before_filter :authenticate_user!
	before_filter :find_post, only: [:show, :edit, :update, :destroy]
	helper_method :sort_column, :sort_direction
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.search(params[:search]).order(sort_column + " " + sort_direction).page(params[:page]).per(5)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to admin_posts_url, notice: 'Post was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update

    respond_to do |format|
      if @post.update_attributes(post_params)
        format.html { redirect_to admin_posts_url, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to admin_posts_url , notice: 'Post was successfully deleted.'}
      format.json { head :no_content }
    end
  end
private
  def post_params
    params.require(:post).permit(:title, :content, :published_at, :slug)
  end

  def find_post
  	@post = Post.find_by_slug!(params[:id])
  end
  def sort_column
  	Post.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end
  def sort_direction
  	%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
