//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require jquery-fileupload/basic
//= require bootstrap-datepicker

$(document).ready(function(){
  $("a[rel=popover]").popover();
  $(".tooltip").tooltip();
  $("a[rel=tooltip]").tooltip();
  $('.datepicker').datepicker({});
});
