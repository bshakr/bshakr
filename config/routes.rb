Bshakr::Application.routes.draw do
  root :to => "main#home"

  devise_for :users

  resources :posts
  get 'about' => 'main#about', as: 'about'
  match '(errors)/:status', to: 'errors#show', constraints: {status: /\d{3}/} , via: :all
  get 'work/solace' => 'work#solace', as: 'solace'
  get 'work/stourvalley' => 'work#stourvalley', as: 'stourvalley'
  get 'work/nooun' => 'work#nooun', as: 'nooun'
  get 'work/nyexpo' => 'work#nyexpo', as: 'nyexpo'
  get 'work/lawfirm' => 'work#lawfirm', as: 'lawfirm'
  get 'work/egyptolution'	=> 'work#egyptolution', as: 'egyptolution'
  get 'work/appommunity'	=> 'work#appommunity', as: 'appommunity'
  get 'work/tia'	=> 'work#tia', as: 'tia'
	get 'work/' => 'work#index', as: 'work'  

  namespace :admin do
  	root :to => 'posts#index'
  	resources :posts
  end
end
