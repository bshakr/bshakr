# bassemshaker.com

This is my site. It's pretty simple. It's running on Ruby 2.0 and Rails 4. Deployed on heroku.

### Get the source 
Note that that you need to have Ruby 2.0 installed. Best way is to install it through https://rvm.io/

Now to get the source, open your command line

    git clone https://github.com/bshakr/bshakr.git
    cd bshakr


Install Postgress and then to setup the database run, you can also change the database name and username in config/database.yml 


    $ psql postgres
    # CREATE USER bassemreda SUPERUSER;
    # \q

Now install all the gems and setup the database.

    bundle install
    rake db:create
    rails server

Now you can open it on localhost:3000