require "minitest_helper"


describe Post do
	it "includes name in to_param" do
		post = Post.create!(:title => "Hello Test", :content => "Hello Test Body")
		post.to_param.must_equal "#{post.id}-hello-test"
	end
end
